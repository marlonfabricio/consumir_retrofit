package com.odpb.consumirretrofit;

import com.google.gson.annotations.SerializedName;


public class Materia {
    @SerializedName("cod_materiapd")
    private String cod_materia;
    @SerializedName("detalle_largo")
    private  String detalle_largo;
    @SerializedName("n_veces_cursadasp")
    private  String n_veces_cursadasp;
    @SerializedName("observaciones")
    private  String observaciones;
    @SerializedName("grupop")
    private  String grupop;
    @SerializedName("programa_destino")
    private  String programa_destino;
    @SerializedName("n_cuantitativa")
    private  String n_cuantitativa;
    @SerializedName("clase_nota")
    private  String clase_nota;

    public String getCod_materia() {
        return cod_materia;
    }

    public void setCod_materia(String cod_materia) {
        this.cod_materia = cod_materia;
    }

    public String getDetalle_largo() {
        return detalle_largo;
    }

    public void setDetalle_largo(String detalle_largo) {
        this.detalle_largo = detalle_largo;
    }

    public String getN_veces_cursadasp() {
        return n_veces_cursadasp;
    }

    public void setN_veces_cursadasp(String n_veces_cursadasp) {
        this.n_veces_cursadasp = n_veces_cursadasp;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getGrupop() {
        return grupop;
    }

    public void setGrupop(String grupop) {
        this.grupop = grupop;
    }

    public String getPrograma_destino() {
        return programa_destino;
    }

    public void setPrograma_destino(String programa_destino) {
        this.programa_destino = programa_destino;
    }

    public String getN_cuantitativa() {
        return n_cuantitativa;
    }

    public void setN_cuantitativa(String n_cuantitativa) {
        this.n_cuantitativa = n_cuantitativa;
    }

    public String getClase_nota() {
        return clase_nota;
    }

    public void setClase_nota(String clase_nota) {
        this.clase_nota = clase_nota;
    }

    @Override
    public String toString() {
        return "Materia{" +
                "cod_materia='" + cod_materia + '\'' +
                ", detalle_largo='" + detalle_largo + '\'' +
                ", n_veces_cursadasp='" + n_veces_cursadasp + '\'' +
                ", observaciones='" + observaciones + '\'' +
                ", grupop='" + grupop + '\'' +
                ", programa_destino='" + programa_destino + '\'' +
                ", n_cuantitativa='" + n_cuantitativa + '\'' +
                ", clase_nota='" + clase_nota + '\'' +
                '}';
    }
}
