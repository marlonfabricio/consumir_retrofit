package com.odpb.consumirretrofit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.R.attr.label;


public class MainActivity extends AppCompatActivity {
    public TextView ver;
    public EditText txtcodigo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ver = (TextView) findViewById(R.id.txtvista);
        txtcodigo = (EditText)findViewById(R.id.codigo);

    }


    public void consultar(View view){
        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://ocara.udenar.edu.co/wservice1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        EstudianteService servicio = retrofit.create(EstudianteService.class);


        Call<Estudiante> call = servicio.getEstudiante(txtcodigo.getText().toString());

        call.enqueue(new Callback<Estudiante>() {
            @Override
            public void onResponse(Call<Estudiante> call, Response<Estudiante> response) {
                Estudiante estudiante = response.body();

                String cadena = "";
                cadena += "Código: " + estudiante.getCodigo() +"\n";
                cadena += "Nombre: " + estudiante.getNombre() +"\n";
                cadena += "Apellido: " + estudiante.getApellido() +"\n";
                cadena += "Semestre: " + estudiante.getSemestre() +"\n";
                cadena += "Programa: " + estudiante.getPrograma() +"\n\n";
                cadena += "Materias matriculadas" +"\n";
                for(int i =0; i<estudiante.getMaterias().size();i++){
                    cadena += "Codigo Materia: " + estudiante.getMaterias().get(i).getCod_materia() +"\n";
                    cadena += "Nombre Materia: " + estudiante.getMaterias().get(i).getDetalle_largo();
                    cadena += "Veces Cursada: " + estudiante.getMaterias().get(i).getN_veces_cursadasp() +"\n";
                    cadena += "Observaciones: " + estudiante.getMaterias().get(i).getObservaciones() +"\n";
                    cadena += "Grupo: " + estudiante.getMaterias().get(i).getGrupop() +"\n";
                    cadena += "Programa Destino: " + estudiante.getMaterias().get(i).getPrograma_destino() +"\n";
                    cadena += "Nota Cuantitativa: " + estudiante.getMaterias().get(i).getN_cuantitativa() +"\n";
                    cadena += "Clase Nota: " + estudiante.getMaterias().get(i).getClase_nota() +"\n";
                    cadena += "-------------------------" +"\n";
                }
                ver.setText(cadena);
            }

            @Override
            public void onFailure(Call<Estudiante> call, Throwable t) {
                ver.setText("casas");
            }
        });
    }
}

