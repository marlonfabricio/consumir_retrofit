package com.odpb.consumirretrofit;


import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;


public interface EstudianteService {
    @GET("getSubject/{codigo}")
    Call<Estudiante> getEstudiante(@Path("codigo") String codigo);
}


